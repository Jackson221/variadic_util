#ifndef NO_TEST

#include "variadic_util.hpp"
#include "test/test.hpp"

template<typename T>
using _single_arg_vector = std::vector<T>;


struct testc
{
	int some_member_data;
	template<typename T>
	void operator()()
	{
		printf("md %d type %s\n",some_member_data,typeid(T).name());
		some_member_data++;
	}
};

using example_tuple = std::tuple<char,int,long,double>;


namespace variadic_util
{
	
	
	void unit_test(test::instance& inst)
	{
		inst.set_name("variadic_util");

		{
			using tuple_of_vectors_of_various = variadic_util::make_variadic_of_template_applied_to_variadic_contents_t<_single_arg_vector, example_tuple>;
			static_assert(std::is_same_v<tuple_of_vectors_of_various, std::tuple<std::vector<char>,std::vector<int>,std::vector<long>,std::vector<double>>>, "make_variadic_of_template_applied_to_variadic_contents_t creates the correct tuple");
		}
		{
			inst.inform("Ensure for_each_type_in_variadic works"); //TODO
			auto thisc = testc{27};
			variadic_util::for_each_type_in_variadic<example_tuple>(thisc);
		}
		{
			inst.inform("Ensure for_each_element_in_variadic works");
			example_tuple filled = {1,2,3,4.1};
			std::string end_result = "";
			std::string expected_result = "1 2 3 4.100000 ";
			variadic_util::for_each_element_in_variadic(filled, [&](auto in){end_result += std::to_string(in) + " ";});
			inst.test(TEST_2(end_result,expected_result, end_result==expected_result,true));
		}
		{
			using v_t = void_permitting_tuple<void,void,int,void>;
			v_t v;
			v.get<2>() = 5;

			static_assert(std::is_same_v<v_t::element_t<1>,void>);
			static_assert(std::is_same_v<v_t::element_t<3>,void>);

			inst.test(TEST_1(v.get<2>(), v.get<2>() == 5, true));

			void_permitting_tuple<> etest;
		}

	}
}

#endif //ifndef NO_TEST
