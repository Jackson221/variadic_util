/*!
 * Provides utilities for working with std::tuple
 *
 * Many of them work for all variadic types as well.
 *
 * Copyright 2019 Jackson Reed McNeill. Licensed under the GNU AGPL version 3 or any later version.
 */
#pragma once

#include <tuple>
#include <type_traits>
#include <utility>
#include <optional>

#ifndef NO_TEST
#include "test/forward.hpp"
#endif

namespace variadic_util
{
	/*!
	 * Useful for when you want to return a type from a function
	 */
	template<typename T> 
	struct type_proxy
	{
		using type = T;
		type_proxy() noexcept
		{
		}
	};
	
	/*!
	 * Useful for when you want to pass a value through as a type through a template
	 */
	template<auto _value>
	struct value_proxy
	{
		static constexpr auto value = _value;
		value_proxy() noexcept {}
	};
	
	/*!
	 * Useful for allowing multiple template-template arguments to be passed into a function
	 * (i.e. emulate variadic template-template arguments)
	 *
	 * Only allows one template argument due to C++ Core Issue 1430: www.open-std.org/jtc1/sc22/wg21/docs/cwg_active.html#1430
	 */
	template<template<typename> typename T>
	struct template_proxy
	{
		template<typename U>
		using type = T<U>;
	};

	//used for templates with 2 args, until the c++ bug is patched
	template<template<typename, typename> typename T>
	struct template_proxy_2
	{
		template<typename U, typename I>
		using type = T<U,I>;
	};
	template<template<typename, auto> typename T>
	struct template_proxy_1_T_1_auto
	{
		template<typename U, auto I>
		using type = T<U,I>;
	};


	//this works by variadic_size calling variadic_size_impl with a tuple_t generated by declval (i.e. one only valid in unevaulated contexts)
	//and then returning the size of the variadic as a type
	//then variadic_size() returns ::value on that type.
	template<template<typename...> typename tuple_t, typename ... T>
	constexpr auto variadic_size_impl(tuple_t<T...>)
	{
		return value_proxy<sizeof...(T)>();
	}
	template<typename T>
	constexpr auto variadic_size()
	{
		return decltype(variadic_size_impl(std::declval<T>()))::value;
	}
	template<typename T>
	constexpr size_t variadic_size_v = variadic_size<T>();

	static_assert(variadic_size_v<std::tuple<int,float>> ==2);
	static_assert(variadic_size_v<std::tuple<>> ==0);
	static_assert(variadic_size_v<std::pair<int,float>> ==2);

	//very similar to how the last one worked, though type_proxy is used instead of declval to make things less confusing
	//
	//essentially, the variadic type has its parameter list shortened `requested` number of times, then the first element of that
	//parameter list is returned (being the element # they requested)
	//
	//At every step, the type is put through type_proxydecltype(..)::type> to avoid any actual evaluation; otherwise the compiler
	//complains that the value of a std::declval is used. This way, these functions are never actually evalulated, only their theoretical
	//return types are used.
	template<size_t requested, size_t current, template<typename...> typename tuple_t, typename T, typename ... U>
	constexpr auto variadic_element_impl(tuple_t<T,U...>)
	{
		if constexpr(requested==current)
		{
			return type_proxy<T>{};
		}
		else
		{
			//re-place the element into the last slot of the template list
			//this is done for compatibility with some non-variadic templates (like std::pair)
			//in order to not change the number of template arguments
			return type_proxy< typename decltype( variadic_element_impl<requested,current+1>(std::declval<tuple_t<U...,T>>()) )::type >();
		}
	}

	template<size_t i, typename T>
		requires (!std::is_const_v<T>)
	constexpr auto variadic_element()
	{
		static_assert(i < variadic_size_v<T>, "variadic must be at least `i` in size (you could not, for example, request element index 3 in a tuple of 3 types)");
		return type_proxy< typename decltype(variadic_element_impl<i,0>(std::declval<T>()))::type >();
	}
	template<size_t i, typename T>
		requires (std::is_const_v<T>)
	constexpr auto variadic_element()
	{
		//return type_proxy< std::add_const_t<typename decltype(variadic_element_impl<i,0>(std::declval<std::remove_const_t<T>>()))::type> >();
		return type_proxy< std::add_const_t<typename decltype( variadic_element<i,std::remove_const_t<T>>() )::type> >();
	}

	template<size_t i, typename T>
	using variadic_element_t = typename decltype(variadic_element<i,T>())::type;

	static_assert(std::is_same_v<variadic_element_t<1,std::pair<int,float>>,float>);
	static_assert(std::is_same_v<variadic_element_t<0,std::tuple<int,float,long>>,int>);
	static_assert(std::is_same_v<variadic_element_t<1,std::tuple<int,float,long>>,float>);
	static_assert(std::is_same_v<variadic_element_t<2,std::tuple<int,float,long>>,long>);
	static_assert(std::is_same_v<variadic_element_t<2,const std::tuple<int,float,long>>,const long>);

	
	template<template<typename...> typename tuple_t, typename ... T>
	constexpr auto get_empty_variadic_impl(tuple_t<T...>)
	{
		return type_proxy<tuple_t<>>();
	}

	template<typename T>
	constexpr auto get_empty_variadic()
	{
		return type_proxy<typename decltype(get_empty_variadic_impl(std::declval<T>()))::type>();
	}

	template<typename T>
	using get_empty_variadic_t = typename decltype(get_empty_variadic<T>())::type;

	static_assert(std::is_same_v<get_empty_variadic_t<std::tuple<int,float,const float>>,std::tuple<>>);


	template<typename new_type, typename ... Y>
	class add_type_to_variadic {};

	template<typename new_type, template<typename...> typename variadic_t, typename ... Y>
	class add_type_to_variadic<new_type, variadic_t<Y...>>
	{
		public:
			using type = variadic_t<Y..., new_type>;
	};

	template<typename new_type, typename variadic_t>
	using add_type_to_variadic_t = typename add_type_to_variadic<new_type, variadic_t>::type;

	static_assert(std::is_same_v<std::tuple<int,float> , add_type_to_variadic_t<float,std::tuple<int>>>);

	template<typename lhs, typename rhs>
	class merge_variadic_types {};

	template<template<typename...> typename lhs_tuple, template<typename...> typename rhs_tuple, typename ... L, typename ... R>
	struct merge_variadic_types<lhs_tuple<L...>, rhs_tuple<R...>>
	{
		using type = lhs_tuple<L..., R...>;
	};
	template<typename lhs_tuple, typename rhs_tuple>
	using merge_variadic_types_t = typename merge_variadic_types<lhs_tuple,rhs_tuple>::type;

	template<typename variadic_t>
		requires (variadic_size_v<variadic_t> == 2)
	using pair_from_variadic_t = std::pair
	<
		variadic_element_t<0, variadic_t>,
		variadic_element_t<1, variadic_t>
	>;
	template<typename pair_t>
	using tuple_from_pair_t = std::tuple
	<
		typename pair_t::first_type,
		typename pair_t::second_type
	>;

	//You can return this type from the template called by make_variadic_of_* utilities in order to have the 
	//type ommitted from the tuple. That will mean the result tuple will be smaller than the input tuple.
	struct ommit_from_tuple {};


	/*!
	 * Create a brand new tuple new_tuple
	 *
	 * Then for every type in order, T, in filled_tuple_t, add a new type into new_tuple which is the result of template_t<T>
	 *
	 * Return new_tuple
	 */
	template<typename template_proxy_t, typename filled_tuple_t, size_t i, typename new_tuple>
	constexpr auto make_variadic_of_proxied_template_applied_to_variadic_contents()
	{
		if constexpr(i == variadic_size_v<filled_tuple_t>)
		{
			return type_proxy<new_tuple>();
		}
		else
		{
			using template_applied_t = typename template_proxy_t::template type<variadic_element_t<i, filled_tuple_t>>;
			if constexpr(std::is_same_v<template_applied_t,ommit_from_tuple>)
			{
				return make_variadic_of_proxied_template_applied_to_variadic_contents<template_proxy_t, filled_tuple_t, i+1, new_tuple>();
			}
			else
			{
				return make_variadic_of_proxied_template_applied_to_variadic_contents<template_proxy_t, filled_tuple_t, i+1, add_type_to_variadic_t<template_applied_t,new_tuple>>();
			}
		};
	}

	template<typename template_proxy_t, typename filled_tuple_t, typename new_tuple = get_empty_variadic_t<filled_tuple_t> >
	using make_variadic_of_proxied_template_applied_to_variadic_contents_t = typename decltype(make_variadic_of_proxied_template_applied_to_variadic_contents<template_proxy_t, filled_tuple_t, 0, new_tuple>())::type;


	template<template<typename> typename template_t, typename filled_tuple_t, typename new_tuple = get_empty_variadic_t<filled_tuple_t> >
	using make_variadic_of_template_applied_to_variadic_contents_t = typename decltype(make_variadic_of_proxied_template_applied_to_variadic_contents<template_proxy<template_t>, filled_tuple_t, 0, new_tuple>())::type;
	

	/*!
	 * Function given will be called on for each type inside the tuple,
	 *
	 * Note that the types given back by your function do NOT have to
	 * be the same as the types in the tuple! 
	 *
	 * Returns a tuple of all the returns given by your function, in 
	 * order.
	 *
	 * TODO: Can I delete this? I think for_each_iterator_make tuple is a better
	 * implementation, maybe at least base this one off it.
	 */
	template<typename tuple_t, typename func_t, size_t i=0>
	inline constexpr auto fill_each_tuple_element(func_t func )
	{
		//TODO effeciency testing
		if constexpr( i == std::tuple_size_v<tuple_t>)
		{
			return std::make_tuple();
		}
		else
		{
			using this_tuple_element = std::tuple_element_t<i,tuple_t>;
			using func_return_t = decltype(func(type_proxy<this_tuple_element>()));

			static_assert(!std::is_same_v<func_return_t,void>, "Cannot return void from function");
			//C++ doesn't define order on function calls. Force it:
			auto first_arg = std::make_tuple(func(type_proxy<this_tuple_element>()));
			auto second_arg = fill_each_tuple_element<tuple_t,func_t,i+1>(func);
			return std::tuple_cat(first_arg,second_arg);
		}
	}
	/*!
	 * Function will be called on each element of the tuple
	 *
	 * Everything returned by the function will be returned by the tuple, in correct order.
	 *
	 * If the function returns void then that type is skipped and the new tuple will be 1 smaller than the given tuple, for each time the function returns void.
	 *
	 * With that in mind, expect possibly a smaller tuple than the given tuple if your function ever returns void.
	 */
	template<typename tuple_t, typename func_t, size_t i=0>
	inline constexpr auto make_tuple_of_function_applied_to_tuple_contents(tuple_t tuple, func_t func )
	{
		//TODO effeciency testing
		if constexpr( i == std::tuple_size_v<tuple_t>)
		{
			return std::make_tuple();
		}
		else
		{
			if constexpr(std::is_same_v< void , decltype(func(std::get<i>(tuple))) > )
			{
				//The user probably still expects their function to run even if it does return void.
				func(std::get<i>(tuple));
				return make_tuple_of_function_applied_to_tuple_contents<tuple_t,func_t,i+1>(tuple,func);
			}
			else
			{
				return std::tuple_cat
				(
					std::make_tuple( func(std::get<i>(tuple)) ),
					make_tuple_of_function_applied_to_tuple_contents<tuple_t,func_t,i+1>(tuple,func)
				);
			}
		}
	}
	template<typename T, typename tuple_t, size_t i>
	concept Size_T_Templatable_Operator_Function = requires (T t, tuple_t tuple)
	{
		{ t. template operator()<i>(std::get<i>(tuple)) };
	};

	template<typename tuple_t, typename func_t>
	inline constexpr auto for_each_element_in_variadic(tuple_t& tuple, func_t func)
	{
		return for_each_iterator<variadic_size_v<tuple_t>>([&]<size_t i>()
		{
			return func(std::get<i>(tuple));
		});
	}

	/* Unfortunately, C++ does not define the order of evaluation for
	 * function arguments so this has to be scrapped.
	template<size_t size, typename func_t>
	inline constexpr auto for_each_iterator_make_tuple([[maybe_unused]] func_t func)
	{
		return 
		(
			[]<std::size_t ... ipack>
			(func_t& f, std::index_sequence<ipack...>)
			{
				return std::make_tuple(f.template operator()<ipack>()...);
			}
		)(func,std::make_index_sequence<size>{});
	}
	*/
	template<size_t size, typename func_t, size_t i=0>
	inline constexpr auto for_each_iterator_make_tuple([[maybe_unused]] func_t func)
	{
		if constexpr(size > i)
		{
			auto result = std::make_tuple(func.template operator()<i>());
			if constexpr( i != size-1)
			{
				return std::tuple_cat( result, for_each_iterator_make_tuple<size,func_t,i+1>(func));
			}
			else
			{
				return result;
			}
		}
		else
		{
			return std::tuple<>{};
		}
	}

	template<typename func_t, size_t ... i>
	inline constexpr auto _helper_for_each_iterator_make_array(func_t func, std::index_sequence<i...>)
	{
		return std::array{func.template operator()<i>()...};
	}


	template<size_t size, typename func_t>
	inline constexpr auto for_each_iterator_make_array(func_t func)
	{
		return _helper_for_each_iterator_make_array(func, std::make_index_sequence<size>());
	}

	/*!
	 * This calls operator() on `func_t`, with the template argument as the current iteration, from `i` to `size` non-inclusive. (like a standard `for (int c=i;c<size;c++)`)
	 *
	 * If operator() returns a non-void type, then the loop will terminate early and return that value. Sort of like `break` in a standard for-loop
	 */
	template<size_t size, typename func_t, size_t i=0>
	inline constexpr auto for_each_iterator([[maybe_unused]] func_t func)
	{
		if constexpr(size > i)
		{
			if constexpr( std::is_same_v<void,  decltype(  func.template operator()<i>()  )> ) //if calling operator() for the next iterator results in `void` type returned
			{
				func.template operator()<i>(); //then simply call it
			}
			else //However, if any non-void type is returned
			{
				//TODO: Should this conceptually be a returnif?
				return func.template operator()<i>(); //Then terminate the loop early, returning that type.
			}
			if constexpr( i != size-1) //Otherwise, keep iterating.
			{
				return for_each_iterator<size,func_t,i+1>(func);
			}
		}
	}

	/*!
	 * Calls your function over the tuple and returns the first std::optional which has a value
	 *
	 * Returns an empty optional if it can't find anything
	 *
	 * Effeciency: O(n)
	 *
	 * Though it is expected for use in a constexpr context where effeciency doesn't really matter
	 */
	template<typename tuple_t, typename func_t, size_t i=0>
		requires (Size_T_Templatable_Operator_Function<func_t,tuple_t,i>)
	inline constexpr std::optional<size_t> search_in_variadic(tuple_t const & tuple, func_t func)
	{
		auto result= func.template operator()<i>(std::get<i>(tuple));
		if(result)
		{
			return result;
		}
		if constexpr(variadic_size_v<tuple_t> != i+1)
		{
			return search_in_variadic<tuple_t,func_t,i+1>(tuple,func);
		}
		return {};
	}

	template<typename tuple_t, typename type>
	inline constexpr std::optional<size_t> index_of_type_in_variadic()
	{
		std::optional<size_t> result;
		for_each_iterator<variadic_size_v<tuple_t>>([&]<size_t i>()
		{
			if constexpr( std::is_same_v< variadic_element_t<i, tuple_t> , type>)
			{
				result = i;
			}
		});

		return result;
	}

	//TODO replace with standard library version
	//pretty sure this is mega incomplete but it works for my purposes and I don't feel like upgrading it since c++20 will have a better version
	template<typename T, typename Y>
	concept equality_comparable_with = requires(T t, Y y)
	{
		{ t == y} -> std::same_as<bool>;
	};

	constexpr auto find_index_of(auto tuple, auto find)
	{
		auto find_tuple_index = [=]<size_t i>(auto in) -> std::optional<size_t>
		{
			if constexpr (equality_comparable_with<decltype(in), decltype(find)>)
			{
				if (in == find)
				{
					return i;
				}
			}
			return {};
		};
		return variadic_util::search_in_variadic(tuple,find_tuple_index);
	}

	template<typename tuple_t, typename class_with_function_run_t>
	inline constexpr auto for_each_type_in_variadic(class_with_function_run_t class_with_function_run)
	{
		return for_each_iterator<variadic_size_v<tuple_t>>([&]<size_t i>()
		{
			return class_with_function_run.template operator()<variadic_element_t<i,tuple_t>>();
		});
	}

	template<template<typename> typename must_meet_concept, typename tuple_t>
	consteval bool _all_tuple_element_meet_concept_fn()
	{
		bool all_types_meet_constraint = true;
		for_each_type_in_variadic<tuple_t>([&]<typename T>() constexpr -> void
		{
			if (!must_meet_concept<T>::value)
			{
				all_types_meet_constraint = false;
			}
		});
		return all_types_meet_constraint;
	}

	//we use c++11 style concepts (i.e. template classes with a bool ::value) because we can't have concept template parameters.
	template<template<typename> typename must_meet_concept, typename tuple_t>
	concept all_tuple_elements_meet_cpp11_concept = _all_tuple_element_meet_concept_fn<must_meet_concept, tuple_t>();

	static_assert(all_tuple_elements_meet_cpp11_concept<std::is_integral, std::tuple<int, char, long long>>);

	template<typename lambda_t>
	inline constexpr auto result_or(lambda_t lambda, [[maybe_unused]] auto alt_val_if_void)
	{
		if constexpr (std::is_same_v<void, decltype( lambda() )>)
		{
			lambda();
			return alt_val_if_void;
		}
		else
		{
			return lambda();
		}
	}

	/*!
	 * A tuple which permits `void` elements.
	 *
	 * Does not specialize std::get or use ADL. Instead, a member function "get" is provided. This is because using the
	 * get function on a member which is void is not allowed, thus this can't really be considered a "normal" tuple.
	 *
	 * Works with variadic_size_v and variadic_element_t though you may use member "size_v" and "element_t" instead.
	 * Because it works with these standard variadic_util utilities, many variadic_util algorithms work with this tuple
	 * (as long as they do not call std::get)
	 */
	template<typename ... values>
	class void_permitting_tuple
	{
		//generate a tuple, given arguments containing void, which will have all arguments
		//except for `void`. 
		template<typename T>
		consteval static auto tuple_without_void()
		{
			if constexpr(!std::is_same_v<T,void>)
			{
				return type_proxy<std::tuple<T>>{};
			}
			else
			{
				return type_proxy<std::tuple<>>{};
			}
		}
		template<typename T, typename ... Y>
			requires(sizeof...(Y) > 0)
		consteval static auto tuple_without_void()
		{
			if constexpr(!std::is_same_v<T,void>)
			{
				return type_proxy<merge_variadic_types_t<std::tuple<T>, typename decltype(tuple_without_void<Y...>())::type>>{};
			}
			else
			{
				return type_proxy<typename decltype(tuple_without_void<Y...>())::type>{};
			}
		}
		using tuple_t = typename decltype(tuple_without_void<values...>())::type;

		tuple_t tuple;

		//given an iterator in the theoretical tuple which contains `void` elements, translate that iterator
		//to the real tuple which does not contain void.
		template<size_t i>
		static consteval size_t find_real_iterator()
		{
			size_t real_it = i;
			for_each_iterator<i>([&real_it]<size_t it>() constexpr
			{
				if constexpr(std::is_same_v<variadic_element_t<it, void_permitting_tuple>, void>)
				{
					real_it--;
				}
			});
			return real_it;
		}
		public:
			template<size_t i>
				requires(!std::is_same_v<variadic_element_t<i,void_permitting_tuple>,void>) //iterator must not be to a `void` element.
			constexpr auto & get() 
			{
				constexpr size_t real_it = find_real_iterator<i>();
				return std::get<real_it>(tuple);
			}
			template<size_t i>
			using element_t = variadic_element_t<i, void_permitting_tuple>;
			
			static constexpr size_t size_v = variadic_size_v<void_permitting_tuple>;
	};
	template<>
	class void_permitting_tuple<>
	{
		public:
			static constexpr size_t size_v = 0;
	};


#ifndef NO_TEST
	void unit_test(test::instance& inst);
#endif


//Source: https://www.scs.stanford.edu/~dm/blog/va-opt.html
//Still has to be prefixed with variadic_util cause namespaces.
#define VARIADIC_UTIL_INTERNAL_PARENS ()

#define VARIADIC_UTIL_INTERNAL_EXPAND(...) VARIADIC_UTIL_INTERNAL_EXPAND4(VARIADIC_UTIL_INTERNAL_EXPAND4(VARIADIC_UTIL_INTERNAL_EXPAND4(VARIADIC_UTIL_INTERNAL_EXPAND4(__VA_ARGS__))))
#define VARIADIC_UTIL_INTERNAL_EXPAND4(...) VARIADIC_UTIL_INTERNAL_EXPAND3(VARIADIC_UTIL_INTERNAL_EXPAND3(VARIADIC_UTIL_INTERNAL_EXPAND3(VARIADIC_UTIL_INTERNAL_EXPAND3(__VA_ARGS__))))
#define VARIADIC_UTIL_INTERNAL_EXPAND3(...) VARIADIC_UTIL_INTERNAL_EXPAND2(VARIADIC_UTIL_INTERNAL_EXPAND2(VARIADIC_UTIL_INTERNAL_EXPAND2(VARIADIC_UTIL_INTERNAL_EXPAND2(__VA_ARGS__))))
#define VARIADIC_UTIL_INTERNAL_EXPAND2(...) VARIADIC_UTIL_INTERNAL_EXPAND1(VARIADIC_UTIL_INTERNAL_EXPAND1(VARIADIC_UTIL_INTERNAL_EXPAND1(VARIADIC_UTIL_INTERNAL_EXPAND1(__VA_ARGS__))))
#define VARIADIC_UTIL_INTERNAL_EXPAND1(...) __VA_ARGS__

#define VARIADIC_UTIL_FOR_EACH(macro, ...)\
	  __VA_OPT__(VARIADIC_UTIL_INTERNAL_EXPAND(VARIADIC_UTIL_INTERNAL_FOR_EACH_HELPER(macro, __VA_ARGS__)))
#define VARIADIC_UTIL_INTERNAL_FOR_EACH_HELPER(macro, a1, ...)\
	  macro(a1)\
	  __VA_OPT__(VARIADIC_UTIL_INTERNAL_FOR_EACH_AGAIN VARIADIC_UTIL_INTERNAL_PARENS (macro, __VA_ARGS__))
#define VARIADIC_UTIL_INTERNAL_FOR_EACH_AGAIN() VARIADIC_UTIL_INTERNAL_FOR_EACH_HELPER

#define VARIADIC_UTIL_FOR_EACH_COMMA_SEPERATED(macro, ...)\
	  __VA_OPT__(VARIADIC_UTIL_INTERNAL_EXPAND(VARIADIC_UTIL_INTERNAL_FOR_EACH_COMMA_SEPERATED_HELPER(macro, __VA_ARGS__)))
#define VARIADIC_UTIL_INTERNAL_FOR_EACH_COMMA_SEPERATED_HELPER(macro, a1, ...)\
	  macro(a1) __VA_OPT__(,)\
	  __VA_OPT__(VARIADIC_UTIL_INTERNAL_FOR_EACH_COMMA_SEPERATED_AGAIN VARIADIC_UTIL_INTERNAL_PARENS (macro, __VA_ARGS__))
#define VARIADIC_UTIL_INTERNAL_FOR_EACH_COMMA_SEPERATED_AGAIN() VARIADIC_UTIL_INTERNAL_FOR_EACH_COMMA_SEPERATED_HELPER


}
