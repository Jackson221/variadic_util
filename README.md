# Variadic\_util Module

Variadic\_util provides various methods for working with variadic types (especially tuples, but also variants and user-defined types) in an easy fashion. 
It is extremely useful, for instance, when you need to iterate over the elements in a tuple, or change the types inside of a tuple.

# Dependancies

Optionally depends upon the test module. Define NO_TEST if building without it.


# Use

Simply add the files to your build system, and provide the parent directory of all depandancies as an include flag (i.e. -I"src/" )

# License

Project is subject to the GNU AGPL version 3 or any later version, AGPL version 3 being found in LICENSE in the project root directory.
